package cal;

public class Fecha {
	
	public int dia;
	public int mes;
	public int anio;
	
	public Fecha() {
		this.dia = 1;
		this.mes = 1;
		this.anio = 1970;
	}

	public Fecha(int dia, int mes, int anio) {
		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
		
		if (!this.esValida()) {
			throw new IllegalArgumentException("fecha inválida: " + this);
		}
	}

	public void imprimir() {
		System.out.println(dia + "/" + mes + "/" + anio);
	}

//	public String toString() {
//		return dia + "/" + mes + "/" + anio;
//	}

	public static boolean esBisiesto(int anio) {
		return anio % 4 == 0 && anio % 100 != 0 || anio % 400 == 0;
	}

	public [static?] int diasDelMes(int mes, int anio) {
		
	}

	private [static?] boolean esValida() {
		
	}

	public [static?] void avanzarDia() {
		
	}

	// TODO
	public [static?] boolean antesQue(Fecha otra) {
		return false;
	}

	// Esto se suele usar sólo para probar cosas,
	// una vez que ven que todo funca bien, bórrenlo.
	public static void main(String[] args) {
		Fecha f1 = new Fecha();
		Fecha f2 = new Fecha(26, 6, 2011);

		System.out.println(f1);
		System.out.println("te fuiste a la B el " + f2);
		
		System.out.println(f2);

		f1.imprimir();
		f2.imprimir();
	}

}
